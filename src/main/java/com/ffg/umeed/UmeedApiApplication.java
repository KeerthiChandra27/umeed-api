package com.ffg.umeed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UmeedApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(UmeedApiApplication.class, args);
	}

}
