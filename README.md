# UMEED-API #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Umeed-api, a spring boot application which runs on server to fetch the content related to Product, trainings, operations etc., data from Database

### How do I get set up? ###

* Clone the project
git clone https://KeerthiChandra27@bitbucket.org/KeerthiChandra27/umeed-api.git
* Import the project as gradle
* Setup the gradle as build tool in Intellij Settings (Cltr+Alt+S).
* Choose gradle wrapper or gradle folder installed in your system.
* Execute the gradle command "gradle clean build" in terminal
* Open the UmeedApiAPplication.java and run it as Spring Boot App.
* By default it runs on localhost:8080

### Minimum Requirements ###
* Java 1.8 and above
* Gradle 4.10.2 (Inbuild with grade wrapper folder)
 

* 

